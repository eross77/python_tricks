# merge two dictionaries
from pprint import pprint
from copy import deepcopy
import json

j1='{"a": 42, "b": "testing", "d": {"sa": "hello", "sx": 33}}'
j2='{"b": "nottesting", "d": {"sb": "bye"}}'

d1 = json.loads(j1)
d2 = json.loads(j2)

#d1 = {'a': 42,
#      'b':'testing',
#      'd': {
#          'sa': 'hello',
#          'sx': 33
#}}

#d2 = {'b': 'nottesting',
#      'd': {
#          'sb': 'bye'
#      }}


def merge(d1, d2, merge=lambda x,y:y):
    """
    Merges two dictionaries, non-destructively, combining
    values on duplicate keys as defined by the optional merge
    function.  The default behavior replaces the values in d1
    with corresponding values in d2.  (There is no other generally
    applicable merge strategy, but often you'll have homogeneous
    types in your dicts, so specifying a merge technique can be
    valuable.)

    Examples:

    >>> d1
    {'a': 1, 'c': 3, 'b': 2}
    >>> merge(d1, d1)
    {'a': 1, 'c': 3, 'b': 2}
    >>> merge(d1, d1, lambda x,y: x+y)
    {'a': 2, 'c': 6, 'b': 4}

    """
    result = dict(d1)
    for k,v in d2.iteritems():
        if k in result:
            result[k] = merge(result[k], v)
        else:
            result[k] = v
    return result

def dict_merge(a, b):
    '''recursively merges dict's. not just simple a['key'] = b['key'], if
    both a and bhave a key who's value is a dict then dict_merge is called
    on both values and the result stored in the returned dictionary.'''
    if not isinstance(b, dict):
        return b
    result = deepcopy(a)
    for k, v in b.iteritems():
        if k in result and isinstance(result[k], dict):
            result[k] = dict_merge(result[k], v)
        else:
            result[k] = deepcopy(v)
    return result

pprint(d1)
pprint(d2)
dd = d1.copy()
dd.update(d2)
print 'dd.update()'
pprint(dd)

print 'merge(d1,d2)'
pprint(merge(d1,d2))

print 'dict_merge(d1,d2)'
pprint(dict_merge(d1,d2))
