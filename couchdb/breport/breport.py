from bottle import route, run, debug, template
import couchdb

@route('/summary')
def todo_list():
    db=couchdb.Server("https://saldb.vcd.hp.com")['pham_backup']
    rows = []
    rowcount = 10
    for i in db.view('_design/proto/_view/alldocs'):
        rowcount = rowcount - 1
        if rowcount <= 0:
            break
        recs=[]
        for rec in i['value']['recs']:
            recs.append(rec)
        if (len(recs) > 2 and type(recs[0]) == dict):
            row= [i['value']['_id'],i['value']['passfail'],i['value']['customdata'],recs]
            rows.append(row)

    output = template('make_table',rows=rows)
    return output

debug(True)
run()