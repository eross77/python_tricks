import couchdb
from pprint import pprint

# create a record if necessary.
# increment the count in the record and save it back.
# print out the result.

s1=couchdb.Server('https://saldb.vcd.hp.com')

db1 = s1['sandbox']

myrec = db1.get('ep10')
if myrec == None:
    myrec = {'_id':'ep10','count': 0}
myrec['count']=myrec['count']+1
myrec['myname']='terry'
db1.save(myrec)
pprint(myrec)