# demonstration of adding a test record with a file attachment.

import couchdb
from resultslogger import Result
from resultslogger.reporters import dbreporter
from tempfile import NamedTemporaryFile
import random

# IGNORE THIS
# this would not normally be in your test program.  It is here to delete the record after you have
# created it.  Note that in the default results db, you will not be able to delete or update a record
# using the normal 'trex' logon.

def delete_record(id):
    svr = couchdb.Server('https://trex:growth@saldb.vcd.hp.com')
    db = svr['eric']
    del db[id]

#
# end IGNORE THIS
#
__NUMRECS = 10
__DELETE_AFTER = False

for i in xrange(__NUMRECS):
    r = Result(app="demo",appversion="0.1",name="result.py test")

    r.log("this is some data")
    r.log("this is some more data")

    # add some custom data
    mycustdata = {'average_value': str(random.randint(0,10)),'low_value': random.randint(32,64)}
    r.customdata = mycustdata

    # create a temporary file, add some data to it, attach it and save it to the db.
    with NamedTemporaryFile(mode='w+b') as f:
        f.write("The random data is %d" % random.randint(0,10))
        f.flush()
        r.attach(f.name)
        # print the result as it will be store.
        print(r.content)
        # store it in the non-standard 'eric' database
        (id, rev) = dbreporter.store(r.content,db='eric')

    print "record stored:",id, rev

    # IGNORE THIS
    # delete the record we just created to avoid demo db clutter.
    if __DELETE_AFTER:
        delete_record(id)

        #end IGNORE THIS
