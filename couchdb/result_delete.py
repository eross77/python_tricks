__author__ = 'ericr'
# delete all results in the "eric" database

import couchdb

db = couchdb.Server('https://saldb.vcd.hp.com')['eric']

rows = db.view('_all_docs')

docs = [{'_id':r.key,'_rev':r.value['rev'],'_deleted':True} for r in rows]

result = db.update(docs)