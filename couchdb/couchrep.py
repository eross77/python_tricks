import couchdb

from pprint import pprint

s1=couchdb.Server()
s2=couchdb.Server('https://saldb.vcd.hp.com')

db1 = s1['sandbox']
db2 = s2['sandbox']

print "Replicating from %s to %s" %(db1.resource.url, db2.resource.url)
result = s1.replicate('sandbox',db2.resource.url)

pprint(result)