__author__ = 'ericr'
import couchdb

svr = couchdb.Server('https://saldb.vcd.hp.com')
db = svr['sandbox']

for i in db:
    if "_design" not in i:
        print "Deleting", i
        db.delete(db[i])

