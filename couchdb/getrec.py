import couchdb
from pprint import pprint as pp
s=couchdb.Server('https://saldb.vcd.hp.com')
db = s['sandbox']
rec = db.get('ep10',conflicts=True)
pp(rec)

pp(rec['_conflicts'])

pp("Conflicts:  ")
for i in rec['_conflicts']:
    crec = db.get('ep10',rev=i)
    pp(crec)
    pp(db.save(crec))

